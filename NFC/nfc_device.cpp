
#include "nfc_device.h"

NFC_Device::NFC_Device(nfc_context *context, QObject *parent) : QObject(parent)
{
    this->context = context;
}

bool NFC_Device::Open(QString connString) {
    // OPEN DEVICE
    this->connectionString = connString;
    this->pnd = nfc_open(context, this->connectionString.toLatin1().data());
    return (pnd!=nullptr);
}

QString NFC_Device::GetDeviceCapabilites() {
    // GET DEVICE CAPABILITIES (chip, supported modulations, etc)
    char *strinfo = NULL;
    QString info = QString();

    if(pnd != nullptr) {
        if(nfc_device_get_information_about(pnd, &strinfo) >= 0) {
            info = QString(strinfo);
            nfc_free(strinfo);
        }
    }

    return info;
}

QString NFC_Device::GetDeviceName() {
    QString name = QString();
    if(pnd != nullptr) {
        name = QString(nfc_device_get_name(pnd));
    }
    return name;
}

bool NFC_Device::InitiatorInit() {
    // INITIALIZE DEVICE AS INITIATOR (READER)
    if(pnd!= nullptr )
        return (nfc_initiator_init(this->pnd) == 0);

    return false;
}

void NFC_Device::Close() {
    if(this->pnd != nullptr)
        nfc_close(this->pnd);
}

bool NFC_Device::AbortCommand() {
    if(this->pnd!=nullptr) {
        return nfc_abort_command(this->pnd);
    }
    return false;
}

bool NFC_Device::isOpen() {
    return(this->pnd != nullptr);
}

int NFC_Device::GetTagOnReader(nfc_target *nt, nfc_modulation nmModulations) {
    nfc_target candidates[MAX_CANDIDATES];

    freeToScan.lock();

    // Drop the field for a while
    nfc_device_set_property_bool(this->pnd, NP_ACTIVATE_FIELD, false);
    // Configure the CRC and Parity settings
    nfc_device_set_property_bool(this->pnd, NP_HANDLE_CRC, true);
    nfc_device_set_property_bool(this->pnd, NP_HANDLE_PARITY, true);
    nfc_device_set_property_bool(this->pnd, NP_AUTO_ISO14443_4, true);
    // Enable field so more power consuming cards can power themselves up
    nfc_device_set_property_bool(this->pnd, NP_ACTIVATE_FIELD, true);

    int res = nfc_initiator_list_passive_targets(this->pnd, nmModulations, candidates, MAX_CANDIDATES);
    //int res = nfc_initiator_select_passive_target(this->pnd, this->nmModulations, NULL, 0, nt);
    if(res > 0 ) {
        *nt = candidates[0];
    }

    freeToScan.unlock();

    return res;
}

QString NFC_Device::UIDToString(nfc_target nt) {
    QByteArray array((char *)nt.nti.nai.abtUid, nt.nti.nai.szUidLen);
    qDebug() << QString(array.toHex());
    return QString(array.toHex());
}

nfc_device *NFC_Device::getRawPND() {
    return this->pnd;
}
