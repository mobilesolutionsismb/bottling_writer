#-------------------------------------------------
#
# Project created by QtCreator 2019-05-13T17:14:22
#
#-------------------------------------------------

QT       += core gui

#CONFIG += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Bottling_Writer
TEMPLATE = app

LIBS += -lusb-1.0 -lnfc -lssl -lcrypto

# install app to /home/root folder on device
target.path = /home/root
INSTALLS += target

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SVN_BASE_DIR = $${PWD}/
DESTDIR += $$SVN_BASE_DIR/build

DIR_NFC = $${PWD}/NFC/
include($$DIR_NFC/NFC.pri)

DIR_LOG = $${PWD}/LOG/
include($$DIR_LOG/LOG.pri)

DIR_USB_NOTIFY = $${PWD}/USB_NOTIFY/
include($$DIR_USB_NOTIFY/USB_NOTIFY.pri)

DIR_NDEF = $${PWD}/NDEF
include($$DIR_NDEF/NDEF.pri)

DIR_NTAG = $${PWD}/NTAG
include($$DIR_NTAG/NTAG.pri)

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        hashinghelpers.cpp \

HEADERS += \
        mainwindow.h \
        hashinghelpers.h \
        datatype_defs.h \

FORMS += \
        mainwindow.ui

RESOURCES += \
    tabdevices.qrc
