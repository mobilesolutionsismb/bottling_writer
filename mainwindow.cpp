#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <ndefmessage.h>
#include <tlv.h>
#include <ntag2xx.h>

#define DEFAULT_URI "https://demo.gcl.cloud/"

#define IN_ONE_GO

QMutex freeToScan;

nfc_context *context;
nfc_target nt;
Ntag2xx *ntag;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //======================== INIT NFC CONTEXT, ONCE AND FOR ALL

    nfc_init(&context);

    if(context == NULL)
    {
        QMessageBox msgBox;
        msgBox.setText("Error initializing NFC context!");
        msgBox.setInformativeText("Try rebooting the app...");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    } else {

        ui->txtNdef->setText(DEFAULT_URI);

        //================================================================ LOGGER
        ui->txtLog->setDisabled(true);
        QPalette p =ui->txtLog->palette();
        p.setColor(QPalette::Base, QColor(255,255,255));
        p.setColor(QPalette::Text, QColor(0,0,0));
        ui->txtLog->setPalette(p);

        logger = new Logger(ui->txtLog);

        //=======================================================================

        //==================================================== USB ASYNC NOTIFIER
        m_usb = new UsbNotifier(0,0,this);
        m_usb->addDeviceFilter(NXP_VID, PN533_PID);
        m_usb->addDeviceFilter(SCM_VID, SCL3711_PID);

        connect(m_usb, SIGNAL(deviceAttached(UsbDevice*)), this, SLOT(deviceAttached(UsbDevice*)));
        connect(m_usb, SIGNAL(deviceDetached(int, int)), this, SLOT(deviceDetached(int, int)));

        deviceMap = m_usb->getDeviceList();
        foreach(QString s, deviceMap.keys()) {
            UsbDevice *ud = deviceMap[s];
            logger->Info(QString("Device present: %1").arg(ud->fullAddress()));
            addDeviceToQList(ud);
        }

        currNfcConnString = QString();
        m_usb->start();

        //=======================================================================

        //====================================================== NFC DEVICES LIST
        currNfcDev = nullptr;
        ui->listWidget->setViewMode(QListView::ListMode);
        ui->listWidget->setIconSize(QSize(24, 24));
        ui->listWidget->setMovement(QListView::Static);
        ui->listWidget->setMaximumWidth(400);
        ui->listWidget->setSpacing(2);
        connect(ui->listWidget, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
                this, SLOT(deviceSelected(QListWidgetItem*,QListWidgetItem*)));

        QState *open = new QState();
        open->setObjectName("open");
        open->assignProperty(ui->btnOpenDevice, "text", "Close Device");
        open->assignProperty(ui->btnOpenDevice, "styleSheet","background-color:rgb(0,255,0);");

        QState *close = new QState();
        close->setObjectName("close");
        close->assignProperty(ui->btnOpenDevice, "text", "Open Device");
        close->assignProperty(ui->btnOpenDevice, "styleSheet","background-color:rgb(190,190,190);");

        close->addTransition(ui->btnOpenDevice, SIGNAL(clicked()), open);
        open->addTransition(this, SIGNAL(forceRemoved()), close);
        open->addTransition(ui->btnOpenDevice, SIGNAL(clicked()), close);

        machine.addState(open);
        machine.addState(close);

        machine.setInitialState(close);
        machine.start();

        connect(open, SIGNAL(entered()), this, SLOT(OpenDevice()));
        connect(close, SIGNAL(entered()), this, SLOT(CloseDevice()));
        //=======================================================================

        //=============================================================== BUTTONS
        connect(ui->btnReadUid, SIGNAL(clicked(bool)), this, SLOT(onReadUidBtnClicked(bool)));
        connect(ui->btnGenerate, SIGNAL(clicked(bool)), this, SLOT(onGenerateBtnClicked(bool)));
        connect(ui->btnWriteTag, SIGNAL(clicked(bool)), this, SLOT(onWriteBtnClicked(bool)));
        //=======================================================================

        //=========================================================== UI ELEMENTS
        ui->iwPass->setFrameShape(QFrame::NoFrame);
        ui->iwPack->setFrameShape(QFrame::NoFrame);
        ui->iwSig->setFrameShape(QFrame::NoFrame);
        ui->iwClose->setFrameShape(QFrame::NoFrame);
        ui->iwOpen->setFrameShape(QFrame::NoFrame);
        ui->ivPass->setFrameShape(QFrame::NoFrame);
        ui->ivPack->setFrameShape(QFrame::NoFrame);
        ui->ivSig->setFrameShape(QFrame::NoFrame);
        ui->ivClose->setFrameShape(QFrame::NoFrame);
        ui->ivOpen->setFrameShape(QFrame::NoFrame);

        ok = QPixmap(":/images/ok.png").scaled(QSize(16,16));
        error = QPixmap(":/images/error.png").scaled(QSize(16,16));

        calculateMirroringString();

        connect(ui->cb_mirrorUid, SIGNAL(stateChanged(int)), this, SLOT(calculateMirroringString()));
        connect(ui->cb_mirrorTT, SIGNAL(stateChanged(int)), this, SLOT(calculateMirroringString()));
        connect(ui->cb_mirrorCounter, SIGNAL(stateChanged(int)), this, SLOT(calculateMirroringString()));
        connect(ui->txtNdef, SIGNAL(textChanged(QString)), this, SLOT(calculateMirroringString()));
        //=======================================================================

    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

//============================================== USB ASYNC NOTIFIER SLOTS
void MainWindow::deviceAttached(UsbDevice *dev) {
    logger->Info(QString("Attached new PN533 reader @ %1").arg(dev->fullAddress()));
    deviceMap.insert(dev->fullAddress(), dev);

    addDeviceToQList(dev);
}

void MainWindow::deviceDetached(int busNumber, int deviceAddress) {
    QString fa = UsbDevice::createFullAddress(busNumber, deviceAddress);
    logger->Warning(QString("Removed PN533 reader @ %1").arg(fa));

    deviceMap.remove(fa);
    removeDeviceFromQList(fa);

    // Check if the removed device was actually open and used in the test
    // if yes, stop the test and close gracefully the libnfc pnd

    // Check if thread is running and connection strings match
    if(currNfcDev!=nullptr && currNfcDev->connectionString.contains(fa)) {
        currNfcDev->abort = true;
        emit forceRemoved();

    }
}
//=======================================================================

//=================================== GUI ELEMENTS UPDATE and INTERACTION
void MainWindow::addDeviceToQList(UsbDevice *dev) {
    QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
    item->setIcon(QIcon(":/images/usb.png"));
    item->setText(QString("%1/%2 - %3").arg(dev->manufacturer(), dev->product(), dev->fullAddress()));
    item->setTextAlignment(Qt::AlignVCenter);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    item->setData(Qt::UserRole, dev->fullAddress());
}

void MainWindow::removeDeviceFromQList(QString fullAddress) {
    // Search for item in QList with fullAddress
    int a = ui->listWidget->count();
    for(int i=0; i< a; i++) {
        QListWidgetItem *item = ui->listWidget->item(i);
        if(item->data(Qt::UserRole) == fullAddress) {
            delete item;
            return;
        }
    }
}

void MainWindow::deviceSelected(QListWidgetItem *curr,QListWidgetItem *prev){
    Q_UNUSED(prev)

    if(curr != nullptr) {
        // ENABLE BUTTONS FOR OPENING DEVICE
        ui->btnOpenDevice->setEnabled(true);
        QString devAddr = curr->data(Qt::UserRole).toString();
        logger->Info("Selected "+devAddr);

        currNfcConnString = devAddr;

    } else {
        // READER WAS DISCONNECTED WHILE SELECTED
        ui->btnOpenDevice->setEnabled(false);
        currNfcConnString = QString();
        //ui->gb_operations->setEnabled(false);
    }

}
//=======================================================================

//=================================================== BUTTONS INTERACTION

void MainWindow::OpenDevice() {
    //CREATE NFC DEVICE REFERENCE
    currNfcDev = new NFC_Device(context);

    //CREATE CONNECTION STRING FROM SELECTED DEVICE LIST
    QString cs = QString("pn53x_usb:%1").arg(currNfcConnString);

    if(currNfcDev->Open(cs) && currNfcDev->InitiatorInit()) {
        logger->Info("NFC device opened");
        // ENABLE BUTTONS FOR OPERATING DEVICE
        ui->gb_operations->setEnabled(true);
    } else {
        logger->Error("Unable to open NFC device");
        ui->btnOpenDevice->animateClick();               // Force state change to CLOSE,
    }
}

void MainWindow::CloseDevice() {
    if(currNfcDev != nullptr) {
        currNfcDev->Close();
        logger->Info("NFC device closed");

        delete currNfcDev;
        currNfcDev = nullptr;
    }

    // DISABLE BUTTONS FOR OPERATING DEVICE
    ui->gb_operations->setEnabled(false);

}

void MainWindow::onReadUidBtnClicked(bool) {
    QElapsedTimer timer;
    timer.start();

    // Get preliminary information about tags in rf range
    nfc_target nt;
    int res = currNfcDev->GetTagOnReader(&nt, Ntag2xx::nmModulations);

    if(res == 0) {
        logger->Warning("No tag in RF range detected");
    } else if(res > 0) {
        logger->Info("Read tag info ok");

        // Try to get NTAG information with GET_VERSION command
        QByteArray version;
        if(Ntag2xx::GetVersion(currNfcDev->getRawPND(), nt.nti.nai, &version) > 0 &&
           Ntag2xx::createTag(version, currNfcDev->getRawPND(), nt, ntag) == 0){
            ui->lblType->setText(ntag->GetTagFriendlyName());
            ui->lblAtqa->setText(QString(ntag->GetATQA().toHex()));
            ui->lblUid->setText(QString(ntag->GetUID().toHex()).toUpper());
            ui->lblSak->setText(QString().setNum(ntag->GetSAK(), 16));

            qDebug() << "Get tag on reader time (ms): " << QString::number(timer.nsecsElapsed()/1000000 ,'f', 2);
            ui->btnGenerate->setEnabled(true);
            ui->btnWriteTag->setEnabled(false);
        } else {
            logger->Error("Tag is not a NTAG type");
        }

    } else {
        logger->Error(QString("Read tag info error with code %1").arg(res));
    }
}

void MainWindow::onGenerateBtnClicked(bool) {
    bool isNegative;
    QString UID = ntag->GetUIDString();

    closeSig = HashingHelpers::GetClosedSignature(UID);
    openSig = HashingHelpers::GetOpenedSignature(UID, &isNegative);
    dynSig = HashingHelpers::GetSignature(UID, isNegative);
    authPwd = HashingHelpers::GetAuthPassword(UID, isNegative);
    packSig = HashingHelpers::GetPackSignature(UID);

    ui->lblCloseSig->setText(HashingHelpers::QByteArrayToHexString(closeSig));
    ui->lblOpenSig->setText(HashingHelpers::QByteArrayToHexString(openSig));
    ui->lblSignature->setText(HashingHelpers::QByteArrayToHexString(dynSig));
    ui->lblPassword->setText(HashingHelpers::QByteArrayToHexString(authPwd));
    ui->lblPackPassword->setText(HashingHelpers::QByteArrayToHexString(packSig));

    ui->btnWriteTag->setEnabled(true);
    ClearSuccessIcons();
}

void MainWindow::onWriteBtnClicked(bool) {
    int res;
    NDEFMessage msg;
    //msg.appendRecord(NDEFRecord::createUriRecord("http://mlbu.co/cbc?cbcid=04132465842135"));
    //msg.appendRecord(NDEFRecord::createUriRecord("https://cmpl.io/q?tag=04132465842135"));
    //msg.appendRecord(NDEFRecord::createUriRecord("http://shareboen.com/?type=pn&uid=04132465842135"));
    //msg.appendRecord(NDEFRecord::createUriRecord("https://demo.gcl.cloud/00000000000000x000000x00000000"));
    msg.appendRecord(NDEFRecord::createUriRecord(ui->lblNdef->toPlainText()));

    // Now we can easily encapsulate it inside a TLV record...
    Tlv tlv1 = Tlv::createNDEFMessageTlv(msg);
    Tlv tlv2 = Tlv::createTerminatorTlv();

    // ...and then we can serialize it and send everywhere.
    QByteArray rawTlv;
    rawTlv.append(tlv1.toByteArray());
    rawTlv.append(tlv2.toByteArray());

    if(ui->cb_auth0->isChecked() || ui->cb_cfglck->isChecked()||ui->cb_cntProtect->isChecked()||
       ui->cb_dynLock->isChecked() || ui->cb_pack->isChecked() || ui->cb_prot->isChecked() ||
            ui->cb_pwd->isChecked() || ui->cb_staticLock->isChecked()) {
        QMessageBox msgBox;
        msgBox.setText("Some locking bits are set!");
        msgBox.setInformativeText("The writing operation will make the tag locked!\nDo you really want to continue?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        int ret = msgBox.exec();

        if(ret == QMessageBox::No)
            return;
    }

    if((res=ntag->ConnectTag()) == 0) {
        QElapsedTimer timer;
        timer.start();

        // =============================================== WRITE NDEF INTO USER MEMORY
        quint8 startPage = ntag->GetFirstUserPageAddress();
        if((res=ntag->WriteNDEF(startPage, rawTlv))<0) {
            logger->Error(QString("NDEF write failed with code %1").arg(res));
            return;
        } else {
            logger->Info("NDEF write ok");
        }
        // ==========================================================================

        // ======================================== WRITE SIGNATURE IN LAST USER PAGE
        if(ui->cb_signature->isChecked()){
            if((res=ntag->WriteUserPage(ntag->GetLastUserPageAddress(), dynSig))<0) {
                logger->Error(QString("SIGNATURE write failed with code %1").arg(res));
                return;
            } else {
                logger->Info("SIGNATURE write ok");
            }
        }
        // ==========================================================================

        // ========================================================= ENABLE MIRRORING
        int idx;
        if(ui->cb_mirrorUid->isChecked() || ui->cb_mirrorCounter->isChecked() || ui->cb_mirrorTT->isChecked()) {
            QString toFind;
            if(ui->cb_mirrorUid->isChecked()) toFind = QString(14, QChar('U'));
            else if(ui->cb_mirrorCounter->isChecked()) toFind = QString(6, QChar('C'));
            else QString(8, QChar('T'));

            idx = rawTlv.indexOf(toFind);

            quint8 m_page, m_byte;
            m_page = idx / Ntag2xx::PageSize + startPage;
            m_byte = idx % Ntag2xx::PageSize;

#ifdef IN_ONE_GO
            // WRITING CFG0 and CFG1 IN ONE GO EXAMPLE
            QByteArray cfg0; quint8 m;
            m = (
                ui->cb_mirrorUid->isChecked()  << 6 |
                ui->cb_mirrorCounter->isChecked()  << 7 |
                m_byte << 4);

            cfg0.append(m);
            cfg0.append('\x00');
            cfg0.append(m_page);
            if(ui->cb_auth0->isChecked())
                cfg0.append(ntag->GetLastUserPageAddress());
            else
                cfg0.append('\xFF');

            if((res=ntag->WriteCFG0Page(cfg0)) < 0) {
                logger->Error(QString("CFG0 write failed with code %1").arg(res));
                return;
            } else
                logger->Info("CFG0 write ok");
#else
            if((res=ntag->SetMirroring(
                m_page,
                m_byte,
                ui->cb_mirrorUid->isChecked(),
                ui->cb_mirrorCounter->isChecked()))<0) {
                    logger->Error(QString("Mirroring failed with code %1").arg(res));
                    return;
            }

            // ENABLE/DISABLE COUNTER FEATURE
            if((res=ntag->EnableCounter(ui->cb_cntEnable->isChecked()))<0) {
                logger->Error(QString("NFC Counter %1able failed with code %2")
                    .arg(ui->cb_cntEnable->isChecked()?"en":"dis")
                    .arg(res));
                return;
            }

            logger->Info("Mirroring feature enable ok");
#endif
        }
        // ==========================================================================
#ifdef IN_ONE_GO
        QByteArray cfg1;
        quint8 a = (
            ui->cb_prot->isChecked() << 7 |
            ui->cb_cfglck->isChecked() << 6 |   // BEWARE! Here is CFGLCK bit!!
            ui->cb_cntEnable->isChecked() << 4 |
            ui->cb_cntProtect->isChecked() << 3);
        cfg1.append(a);
        if((res=ntag->WriteCFG1Page(cfg1))<0) {
            logger->Error(QString("CFG1 write failed with code %1").arg(res));
            return;
        } else
            logger->Info("CFG1 write ok");
#else
        // =================================================== ENABLE AUTH0 (in CFG0)
        if(ui->cb_auth0->isChecked()) {
            if((res=ntag->SetAuth0(ntag->GetLastUserPageAddress()))<0) {
                logger->Error(QString("Set AUTH0 failed with code %1").arg(res));
                return;
            }
        }
        // ==========================================================================

        // ==================================================== ACCESS BYTE (in CFG1)
        if(ui->cb_prot->isChecked() || ui->cb_cfglck->isChecked() || ui->cb_cntProtect->isChecked()) {
            if((res=ntag->SetAccess(ui->cb_prot->isChecked(), ui->cb_cfglck->isChecked(), ui->cb_cntProtect->isChecked()))<0) {
                logger->Error(QString("Set ACCESS failed with code %1").arg(res));
                return;
            }
        }
        // ==========================================================================
#endif       
        // =============================================================== PWD & PACK
        if(ui->cb_pwd->isChecked()) {
            if((res = ntag->WritePWD(authPwd))<0) {
                logger->Error(QString("Set PWD failed with code %1").arg(res));
                return;
            } else
                logger->Info("PWD writing ok");
        }

        if(ui->cb_pack->isChecked()) {
            if((res = ntag->WritePACK(packSig))<0) {
                logger->Error(QString("Set PACK failed with code %1").arg(res));
                return;
            } else
                logger->Info("PACK writing ok");
        }
        // ==========================================================================

        // =============================================================== LOCK BYTES
        if(ui->cb_staticLock->isChecked()) {
            if((res=ntag->SetStaticLock(0xff, 0xff))<0) {
                logger->Error(QString("Set Static Lock failed with code %1").arg(res));
                return;
            } else
                logger->Info("STATIC lock writing ok");
        }

        if(ui->cb_dynLock->isChecked()) {
            if((res=ntag->SetDynamicLock(0xFF, 0x0F, 0x3F))<0) {
                logger->Error(QString("Set Dynamic Lock failed with code %1").arg(res));
                return;
            } else
                logger->Info("DYNAMIC lock writing ok");
        }
        // ==========================================================================

        qDebug() << "Complete write time (ms): " << QString::number(timer.nsecsElapsed()/1000000 ,'f', 2);

        // OTHER FUNCTIONS EXAMPLES
        //QByteArray readData;
        //ntag->ReadUserMemory(readData);
        //ntag->Read1Page(0x05, readData);
        //ntag->Read4Page(0x05, readData);
        //quint32 v = ntag->ReadCounter();

        ntag->DisconnectTag();
    } else
        logger->Error(QString("Unable to communicate with tag (code %1)").arg(res));

}
//=======================================================================

//===================================================MIRRORING CHECKBOXES
void MainWindow::calculateMirroringString() {
    QString value = ui->txtNdef->text();

    //Add UID mirror
    if(ui->cb_mirrorUid->isChecked()) {
        value.append(QString(14, QChar('U')));
    }

    //Add COUNTER mirror
    if(ui->cb_mirrorCounter->isChecked()) {
        if(ui->cb_mirrorUid->isChecked()) value.append('x');
        value.append(QString(6, QChar('C')));

        //Enable the NFC_CNT_EN checkbox too
        ui->cb_cntEnable->setChecked(true);

    } else {
        //Disable the NFC_CNT_EN checkbox too
        ui->cb_cntEnable->setChecked(false);
    }

    //Add TAG_TAMPER mirror
    if(ui->cb_mirrorTT->isChecked()) {
        if(ui->cb_mirrorUid->isChecked()||ui->cb_mirrorCounter->isChecked()) value.append('x');
        value.append(QString(8, QChar('T')));
    }

    ui->lblNdef->setPlainText(value);
}
//=======================================================================

// ICONS FUNCTIONS
void MainWindow::SetSuccessIcons(QLabel *l_write, QLabel *l_verify, int res) {
    // Remove image (reset)
    if(res == -4) {l_write->clear(); l_verify->clear(); return;}

    // Unable to write
    if(res == -1) {l_write->setPixmap(error); return;}

    // Unable to read or // Verify mismatch
    if(res <= -2) {l_write->setPixmap(ok); l_verify->setPixmap(error); return;}

    l_write->setPixmap(ok); l_verify->setPixmap(ok); return;

}

void MainWindow::ClearSuccessIcons() {
    SetSuccessIcons(ui->iwClose, ui->ivClose, -4);
    SetSuccessIcons(ui->iwOpen, ui->ivOpen, -4);
    SetSuccessIcons(ui->iwSig, ui->ivSig, -4);
    SetSuccessIcons(ui->iwPack, ui->ivPack, -4);
    SetSuccessIcons(ui->iwPass, ui->ivPass, -4);
}
