#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <datatype_defs.h>

#include <QMainWindow>
#include <QMessageBox>
#include <QElapsedTimer>
#include <QTimer>
#include <QListWidgetItem>
#include <QState>
#include <QStateMachine>
#include <QPixmap>

#include <nfc/nfc.h>

#include "NFC/nfc_device.h"

#include "LOG/logger.h"

#include "USB_NOTIFY/usbnotifier.h"
#include "USB_NOTIFY/usbdevice.h"

#include "hashinghelpers.h"

#define NXP_VID     0x04CC
#define PN533_PID   0x2533

#define SCM_VID     0x04E6
#define SCL3711_PID 0x5591

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    Logger *logger;

    QStateMachine machine;

    QPixmap ok,error;

    UsbNotifier *m_usb;
    QMap<QString, UsbDevice *> deviceMap;

    QString currNfcConnString;
    nfc_context *context;
    NFC_Device *currNfcDev;

    QByteArray openSig, closeSig, dynSig, packSig, authPwd;

signals:
    void forceRemoved();

private slots:
    void addDeviceToQList(UsbDevice *dev);
    void removeDeviceFromQList(QString fullAddress);
    void deviceSelected(QListWidgetItem *curr,QListWidgetItem *prev);
    void deviceAttached(UsbDevice *dev);
    void deviceDetached(int busNumber, int deviceAddress);

    void calculateMirroringString();

public slots:
    void OpenDevice();
    void CloseDevice();
    void onReadUidBtnClicked(bool);
    void onGenerateBtnClicked(bool);
    void onWriteBtnClicked(bool);

    void ClearSuccessIcons();
    void SetSuccessIcons(QLabel *l_w, QLabel *l_v, int res);
};

#endif // MAINWINDOW_H
