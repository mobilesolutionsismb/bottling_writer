SOURCES += \
    $$PWD/tlv.cpp \
    $$PWD/ndefrecord.cpp \
    $$PWD/ndefmessage.cpp \
    $$PWD/ndefrecordtype.cpp

HEADERS += \
    $$PWD/tlv.h \
    $$PWD/ndefrecord.h \
    $$PWD/ndefmessage.h \
    $$PWD/ndefrecordtype.h \
    $$PWD/libndef_global.h

INCLUDEPATH += \
    $${PWD}
