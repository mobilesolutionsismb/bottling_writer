#ifndef HASHINGHELPERS_H
#define HASHINGHELPERS_H

#include <QString>
#include <QCryptographicHash>
#include <QDataStream>
#include <QDebug>
#include <QByteArray>

/* EXAMPLES */
/*
    UID;Signature;PackSignature;AuthPassword;OpenedSignature;ClosedSignature
    0499DF02D36380;E350ADAC;71B4;433CDAD9;4BB1D73F;20414740
    0421B04AD16381;72C5925C;7F8D;A9D752AF;2ADC7A98;6890A8A6
    04983202D36380;C93C9778;3133;9CA2D821;453CD0E1;5540A740
    0498CF02D36380;54EB3427;504C;5B88EBA6;7863B05D;0AB4CCB7
*/

class HashingHelpers
{
public:
    enum HashLength{INT32 = 4, INT16 = 2};
        Q_ENUMS(HashLength)

    static QByteArray GetSignature(QString UID, bool openedSignatureSign);
    static QByteArray GetPackSignature(QString UID);
    static QByteArray GetAuthPassword(QString UID, bool openedSignatureSign);
    static QByteArray GetOpenedSignature(QString UID, bool *isNegative);
    static QByteArray GetClosedSignature(QString UID);

    static int GetHash(QString UID, QString password, QCryptographicHash::Algorithm alg, HashLength l);
    static QByteArray IntToQByteArray(int v, HashLength l, QDataStream::ByteOrder endianess=QDataStream::LittleEndian);
    static int QByteArrayToInt(QByteArray ba, HashLength l, QDataStream::ByteOrder endianess=QDataStream::LittleEndian);
    static QString IntToHexString(int v, HashLength l);
    static QString QByteArrayToHexString(QByteArray ba);

private:

    static const QString password1;
    static const QString password2;
    static const QString password3;
    static const QString password4;
    static const QString password5;
};

#endif // HASHINGHELPERS_H
