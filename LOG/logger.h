#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QTextEdit>
#include <QTextBlock>
#include <QLabel>
#include <QTextStream>
#include <QDateTime>

Q_DECLARE_METATYPE(QTextBlock)
Q_DECLARE_METATYPE(QTextCursor)

class Logger : public QObject
{
    Q_OBJECT

public:
    explicit Logger(QTextEdit *txtLog, QString logFilePath = NULL, QObject *parent = nullptr);

    bool OpenLogFile();
    void Info(QString s);
    void Warning(QString s);
    void Error(QString s);

    //void Info_Hex(const uint8_t *pbtData, const size_t szBytes, bool newLine=true);

signals:
    void scroll();

public slots:
    void scrollToCaret();

private:
    QTextEdit *logWidget;
    QString logFilePath;
    QTextStream *logStream;



};

#endif // LOGGER_H
