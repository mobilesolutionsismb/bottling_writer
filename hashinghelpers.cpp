#include "hashinghelpers.h"

const QString HashingHelpers::password1 = "@9gl03D4e5F6g7H8";
const QString HashingHelpers::password2 = "$bml08p!&awenbQbQb";
const QString HashingHelpers::password3 = "bmplg234kkjsKJj!asj";
const QString HashingHelpers::password4 = "49flidakl123exWsi1aWWji!!nmnsi90";
const QString HashingHelpers::password5 = "$v0km12!&aw11bQcQb";


QByteArray HashingHelpers::GetClosedSignature(QString UID) {
    int v = GetHash(UID, password1, QCryptographicHash::Algorithm::Sha256, INT32);
    if(v < 0)
        v *= -1;

    return IntToQByteArray(v, INT32, QDataStream::BigEndian);

    /*

    QString a = QString("%1").arg((quint32)v, INT32*2, 16, QChar('0'));
    //qDebug() << "Closed Signature: " << a.toUpper();
    return a;
    */
}

QByteArray HashingHelpers::GetOpenedSignature(QString UID, bool *isNegative) {
    int v = GetHash(UID, password2, QCryptographicHash::Algorithm::Sha256, INT32);
    if(v < 0) {
        *isNegative = true;
        v *= -1;
    }

    return IntToQByteArray(v, INT32, QDataStream::BigEndian);
    /*
    QString a = QString("%1").arg((quint32)v, INT32*2, 16, QChar('0'));
    //qDebug() << "  Open Signature: " << a.toUpper();
    return a;
    */
}

QByteArray HashingHelpers::GetSignature(QString UID, bool openedSignatureSign) {
    int v = GetHash(UID, password3, QCryptographicHash::Algorithm::Sha256, INT32);
    if(openedSignatureSign)
        v*= -1;

    return IntToQByteArray(v, INT32, QDataStream::BigEndian);
    /*
    QString a = QString("%1").arg((quint32)v, INT32*2, 16, QChar('0'));
    //qDebug() << "       Signature: " << a.toUpper();
    return a;
    */
}

QByteArray HashingHelpers::GetAuthPassword(QString UID, bool openedSignatureSign) {
    int v = GetHash(UID, password4, QCryptographicHash::Algorithm::Sha256, INT32);
    if(openedSignatureSign)
        v*= -1;

    return IntToQByteArray(v, INT32, QDataStream::BigEndian);
    /*
    QString a = QString("%1").arg((quint32)v, INT32*2, 16, QChar('0'));
    //qDebug() << "   Auth Password: " << a.toUpper();
    return a;
    */
}

QByteArray HashingHelpers::GetPackSignature(QString UID) {
    int v = GetHash(UID, password5, QCryptographicHash::Algorithm::Md5, INT16);
    if(v < 0)
        v *= -1;

    return IntToQByteArray(v, INT16, QDataStream::BigEndian);
    /*
    QString a = QString("%1").arg((quint16)v, INT16*2, 16, QChar('0'));
    //qDebug() << "  Pack Signature: " << a.toUpper();
    return a;
    */
}

int HashingHelpers::GetHash(QString UID, QString password, QCryptographicHash::Algorithm alg, HashLength l) {

    UID.append(password);

    QByteArray hashBytes = QCryptographicHash::hash(
        UID.toUtf8(), alg)
        .mid(0,l);

    return QByteArrayToInt(hashBytes, l);
}

int HashingHelpers::QByteArrayToInt(QByteArray ba, HashLength l, QDataStream::ByteOrder endianess) {
    qint32 v32; qint16 v16;
    QDataStream ds(ba);
    ds.setByteOrder(endianess);

    if(l == INT32) {
        ds >> v32;
        return (int)v32;
    } else {
        ds >> v16;
        return (int)v16;
    }
}

QByteArray HashingHelpers::IntToQByteArray(int v, HashLength l, QDataStream::ByteOrder endianess) {
    QByteArray ba;
    QDataStream ds(&ba, QIODevice::WriteOnly);
    ds.setByteOrder(endianess);

    if(l == INT32) {
        ds << (qint32)v;
    } else {
        ds << (qint16)v;
    }

    return ba;
}

QString HashingHelpers::IntToHexString(int v, HashLength l) {
    return QString("%1").arg(v, l*2, 16, QChar('0')).toUpper();
}

QString HashingHelpers::QByteArrayToHexString(QByteArray ba) {
    return ba.toHex().toUpper();
}
