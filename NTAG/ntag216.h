#ifndef Ntag216_H
#define Ntag216_H

#include <QObject>
#include <ntag2xx.h>

class Ntag216 : public Ntag2xx
{
public:    
    Ntag216(nfc_device *pnd, nfc_target nt);

    virtual int GetTotalMemorySize();
    virtual int GetTotalMemoryPages();

    virtual int GetUserMemorySize();
    virtual int GetUserMemoryPages();
    virtual int GetFirstUserPageAddress();
    virtual int GetLastUserPageAddress();
    virtual int GetLastPage();

    virtual NTAGType GetTagType();
    virtual QString GetTagFriendlyName();

    virtual quint32 ReadCounter();

    virtual int SetStaticLock(quint8 lock_byte0, quint8 lock_byte1);
    virtual int SetDynamicLock(quint8 dyn_lock_byte0, quint8 dyn_lock_byte1, quint8 dyn_lock_byte2);
    virtual int SetMirroring(quint8 page, quint8 byte, bool uid, bool cnt, bool tt = false);
    virtual int SetAuth0(quint8 from);
    virtual int EnableCounter(bool enable);
    virtual int SetAccess(bool prot, bool cfglck, bool nfc_cnt_prot);

    virtual int WritePWD(QByteArray pwd);
    virtual int WritePACK(QByteArray pack);

    virtual int WriteCFG0Page(QByteArray data);
    virtual int WriteCFG1Page(QByteArray data);

private:
    enum PagesAddr {
        UID_1            = 0x00,
        UID_2            = 0x01,
        LOCK             = 0x02,
        CC               = 0x03,
        FIRST_USER_PAGE  = 0x04,
        LAST_USER_PAGE   = 0xE1,
        DYN_LOCK         = 0xE2,
        CFG_0            = 0xE3,
        CFG_1            = 0xE4,
        PWD              = 0xE5,
        PACK             = 0xE6
    };

    enum MirrorType {
        M_BYTE_OFFSET   = 4,
        M_UID_OFFSET    = 6,
        M_CNT_OFFSET    = 7,
    };

    enum AccessBits {
        A_PROT              = 7,
        A_CFGLCK            = 6,
        A_NFC_CNT_EN        = 4,
        A_NFC_CNT_PWD_PROT  = 3,
    };

    const int totalMemoryPages = 231;
    const int CC_userMemoryPages = 0x6D;

};

#endif // Ntag216_H
