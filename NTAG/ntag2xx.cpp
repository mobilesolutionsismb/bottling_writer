#include "ntag2xx.h"

#include "ntag213.h"
#include "ntag215.h"
#include "ntag216.h"


#define NTAG_TRANSCEIVE(cmd, res, res_len) do { \
        int ret; \
        if ((ret = nfc_initiator_transceive_bytes (this->pnd, (uint8_t*)cmd.data(), cmd.length(), res, res_len, 0)) < 0) {\
            return -EIO;\
        }\
    } while(0)

#define NTAG_TRANSCEIVE_RAW(cmd, res, res_len) do { \
        int ret; \
        if (nfc_device_set_property_bool (this->pnd, NP_EASY_FRAMING, false) < 0) return -EIO; \
        if ((ret = nfc_initiator_transceive_bytes (this->pnd, (uint8_t*)cmd.data(), cmd.length(), res, res_len, 0)) < 0) {\
            return -EIO;\
        }\
        if (nfc_device_set_property_bool (this->pnd, NP_EASY_FRAMING, true) < 0) return -EIO; \
    } while(0)


Ntag2xx::Ntag2xx(nfc_device *pnd, nfc_target nt, QObject *parent) : QObject(parent)
{
    this->nt = nt;
    this->pnd = pnd;
}

int Ntag2xx::createTag(QByteArray version, nfc_device *pnd, nfc_target nt, Ntag2xx *&newTag) {
    int ret = 0;
    newTag = nullptr;

    if(version[VENDOR_ID] != '\x04' || version[PRODUCT_TYPE] != '\x04' ) ret = -EINVAL;

    if(version[PRODUCT_SUBTYPE] == '\x01') {
        switch(version[STORAGE_SIZE]) {
            //case 0x0B: newTag = new Ntag210(pnd, nt); break;
            //case 0x0E: newTag = new Ntag212(pnd, nt); break;
            default: ret = -EINVAL;
        }
    }
    else if(version[MAJOR_VER] == '\x01'){
        switch(version[STORAGE_SIZE]) {
            case 0x0F: newTag = new Ntag213(pnd, nt); break;
            case 0x11: newTag = new Ntag215(pnd, nt); break;
            case 0x13: newTag = new Ntag216(pnd, nt); break;
            default: ret = -EINVAL;
        }
    } else {
        //newTag = new Ntag213TT(pnd, nt);
    }
    return ret;
}

int Ntag2xx::GetVersion(nfc_device *device, nfc_iso14443a_info nai, QByteArray *data) {
    int ret;
    uint8_t cmd[] = {0x60};
    uint8_t res[8];

    nfc_initiator_select_passive_target(device, nmModulations, nai.abtUid, nai.szUidLen, NULL);
    nfc_device_set_property_bool(device, NP_EASY_FRAMING, false);
    ret = nfc_initiator_transceive_bytes(device, cmd, sizeof(cmd), res, sizeof(res), 0);
    nfc_device_set_property_bool(device, NP_EASY_FRAMING, true);
    nfc_initiator_deselect_target(device);

    QByteArray ba(reinterpret_cast<const char*>(res), ret);
    *data = ba;
    return ret;
}

QByteArray Ntag2xx::GetATQA() {
    QByteArray a1(reinterpret_cast<const char *>(nt.nti.nai.abtAtqa), 2);
    return a1;
}

QByteArray Ntag2xx::GetUID() {
    QByteArray a2(reinterpret_cast<const char *>(nt.nti.nai.abtUid), nt.nti.nai.szUidLen);
    return a2;
}

QString Ntag2xx::GetUIDString() {
    return GetUID().toHex().toUpper();
}

byte Ntag2xx::GetSAK() {
    return (byte)nt.nti.nai.btSak;
}

// NTAG COMMANDS
int Ntag2xx::ConnectTag() {
    if(nfc_initiator_select_passive_target(this->pnd, this->nmModulations, nt.nti.nai.abtUid, nt.nti.nai.szUidLen, NULL) < 0)
        return -EIO;
    return 0;
}

int Ntag2xx::DisconnectTag() {
    if(nfc_initiator_deselect_target(this->pnd) < 0)
        return -EIO;
    return 0;
}

// USER MEMORY FUNCTIONS
int Ntag2xx::EraseUserMemory() {
    int ret;
    QByteArray data(4, (char)0);
    for(quint8 a=GetFirstUserPageAddress(); a <= GetLastUserPageAddress(); a++) {
        if((ret = WritePage(a, data)) < 0)
            return ret;
    }
    return 0;
}

int Ntag2xx::ReadUserMemory(QByteArray &data) {
    return FastRead(GetFirstUserPageAddress(), GetLastUserPageAddress(), data);
}

int Ntag2xx::ReadUserPage(quint8 page, QByteArray &data) {
    if(page >= GetFirstUserPageAddress() && page<= GetLastUserPageAddress())
        return Read1Page(page,data);
    else return -EFAULT;
}

int Ntag2xx::WriteUserPage(quint8 page, QByteArray data) {
    if(page >= GetFirstUserPageAddress() && page<= GetLastUserPageAddress())
        return WritePage(page,data);
    else return -EFAULT;
}

int Ntag2xx::WriteNDEF(quint8 startPage, QByteArray data) {
    int bytesToWrite = data.length();
    int ret;
    do {
        QByteArray chunk = data.left(PageSize);
        if((ret = WritePage(startPage, chunk)) < 0)
            return ret;

        data.remove(0, PageSize);
        bytesToWrite = data.length();
        startPage++;
    } while(bytesToWrite > 0);
    return 0;
}



// PRIVATE FUNCTIONS
int Ntag2xx::WritePage(quint8 page, QByteArray cmd) {
    if(page > GetLastPage())
        return -EFAULT;

    if(cmd.length()> PageSize)
        return -EPERM;

    // Pad data to a 4 byte length
    if(cmd.length() < PageSize) {
        cmd.append(PageSize - cmd.length(), 0);
    }

    cmd.prepend(page);
    cmd.prepend(0xA2);

    uint8_t res[1];
    NTAG_TRANSCEIVE(cmd, res, 1);
    return 0;
}

int Ntag2xx::Read4Page(quint8 page, QByteArray &data) {
    QByteArray cmd("\x30");
    cmd.append(page);
    uint8_t *res = (uint8_t *)malloc(4 * PageSize * sizeof(uint8_t));

    NTAG_TRANSCEIVE(cmd, res, 4 * PageSize);

    data = QByteArray(reinterpret_cast<const char *>(res), 4 * PageSize);
    return 0;
}

int Ntag2xx::Read1Page(quint8 page, QByteArray &data) {
    QByteArray tempData;
    int ret = Read4Page(page, tempData);

    if(ret == 0)
        data = tempData.mid(0, PageSize);

    return ret;
}

int Ntag2xx::FastRead(quint8 startPage, quint8 stopPage, QByteArray &data) {
    int memSize = (stopPage - startPage +1) * PageSize;

    QByteArray cmd("\x3A");
    cmd.append(startPage);
    cmd.append(stopPage);

    uint8_t *res = (uint8_t *)malloc(memSize * sizeof(uint8_t));

    NTAG_TRANSCEIVE_RAW(cmd, res, memSize);

    data = QByteArray(reinterpret_cast<const char *>(res), memSize);
    return 0;
}

nfc_modulation Ntag2xx::nmModulations = { .nmt = NMT_ISO14443A, .nbr = NBR_106 };
