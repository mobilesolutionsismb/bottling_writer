#ifndef NTAG2XX_H
#define NTAG2XX_H

#include <QObject>
#include <QDebug>
#include <QBitArray>
#include <datatype_defs.h>
#include <nfc/nfc.h>


class Ntag2xx : public QObject
{
    Q_OBJECT
public:
    enum NTAGType {
        NTAG_210,
        NTAG_212,
        NTAG_213,
        NTAG_215,
        NTAG_216,
        NTAG_213TT,
    };

    enum CMD_GET_VERSION_OFFSETS {
        VENDOR_ID = 1,
        PRODUCT_TYPE = 2,
        PRODUCT_SUBTYPE = 3,
        MAJOR_VER = 4,
        MINOR_VER = 5,
        STORAGE_SIZE = 6,
    };

    explicit Ntag2xx(nfc_device *pnd, nfc_target nt, QObject *parent = nullptr);


    static const int   PageSize = 4;     // Number of bytes for each page
    virtual int GetTotalMemorySize() = 0;
    virtual int GetTotalMemoryPages() = 0;

    virtual int GetUserMemorySize() = 0;
    virtual int GetUserMemoryPages() = 0;
    virtual int GetFirstUserPageAddress() = 0;
    virtual int GetLastUserPageAddress() = 0;

    virtual int GetLastPage() = 0;

    QByteArray  GetATQA();
    QByteArray  GetUID();
    QString     GetUIDString();
    byte        GetSAK();

    virtual QString     GetTagFriendlyName() = 0;
    virtual NTAGType    GetTagType() = 0;

    static int createTag(QByteArray version, nfc_device *pnd, nfc_target nt, Ntag2xx *&newTag);
    static int GetVersion(nfc_device *device, nfc_iso14443a_info nai, QByteArray *data);


    int     ConnectTag();
    int     DisconnectTag();
    int     EraseUserMemory();
    int     ReadUserMemory(QByteArray &data);
    int     WriteUserPage(quint8 page, QByteArray data);
    int     ReadUserPage(quint8 page, QByteArray &data);
    int     WriteNDEF(quint8 startPage, QByteArray data);

    virtual quint32 ReadCounter() = 0;

    virtual int SetStaticLock(quint8 lock_byte0, quint8 lock_byte1) = 0;
    virtual int SetDynamicLock(quint8 dyn_lock_byte0, quint8 dyn_lock_byte1, quint8 dyn_lock_byte2) = 0;
    virtual int SetMirroring(quint8 page, quint8 byte, bool uid, bool cnt=false, bool tt=false) = 0;
    virtual int SetAuth0(quint8 from) = 0;
    virtual int EnableCounter(bool enable) = 0;
    virtual int SetAccess(bool prot, bool cfglck, bool nfc_cnt_en) = 0;

    virtual int WriteCFG0Page(QByteArray data)=0;
    virtual int WriteCFG1Page(QByteArray data)=0;

    virtual int WritePWD(QByteArray pwd)=0;
    virtual int WritePACK(QByteArray pack)=0;

    static nfc_modulation nmModulations;

protected:
    nfc_target nt;
    nfc_device *pnd;

    int     FastRead(quint8 startPage, quint8 stopPage, QByteArray &data);
    int     Read4Page(quint8 page, QByteArray &data);
    int     Read1Page(quint8 page, QByteArray &data);
    int     WritePage(quint8 page, QByteArray data);

signals:

public slots:
};

#endif // NTAG2XX_H
