SOURCES += \
    $$PWD/ntag2xx.cpp \
    $$PWD/ntag213.cpp \
    $$PWD/ntag215.cpp \
    $$PWD/ntag216.cpp

HEADERS += \
    $$PWD/ntag2xx.h \
    $$PWD/ntag213.h \
    $$PWD/ntag215.h \
    $$PWD/ntag216.h

INCLUDEPATH += \
    $${PWD}
