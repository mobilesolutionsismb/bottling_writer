#include "ntag213.h"

Ntag213::Ntag213(nfc_device *pnd, nfc_target nt)
    :Ntag2xx(pnd, nt)
{

}

int Ntag213::GetTotalMemoryPages() { return totalMemoryPages; }
int Ntag213::GetTotalMemorySize() { return totalMemoryPages * 8; }
int Ntag213::GetUserMemorySize() { return GetUserMemoryPages() * 8; }
int Ntag213::GetUserMemoryPages() { return CC_userMemoryPages; }
int Ntag213::GetFirstUserPageAddress() { return FIRST_USER_PAGE; }
int Ntag213::GetLastUserPageAddress() { return LAST_USER_PAGE; }
int Ntag213::GetLastPage() {return PACK;}

QString Ntag213::GetTagFriendlyName() { return QString("NTAG213");}
Ntag2xx::NTAGType Ntag213::GetTagType(){ return Ntag2xx::NTAG_213;}

quint32 Ntag213::ReadCounter() {
    QByteArray cmd("\x39\x02"); int ret;
    uint8_t *res = (uint8_t *)malloc(4 * sizeof(uint8_t));

    if (nfc_device_set_property_bool (this->pnd, NP_EASY_FRAMING, false) < 0) return -EIO;
    if ((ret = nfc_initiator_transceive_bytes (this->pnd, (uint8_t*)cmd.data(), cmd.length(), res, 3, 0)) < 0) {
        return -EIO;
    }
    if (nfc_device_set_property_bool (this->pnd, NP_EASY_FRAMING, true) < 0) return -EIO;

    return *(quint32*)res;
}

int Ntag213::SetStaticLock(quint8 lock_byte0, quint8 lock_byte1) {
    QByteArray lb(2, (char)0);
    lb.append(lock_byte0);
    lb.append(lock_byte1);
    return WritePage(LOCK, lb);
}

int Ntag213::SetDynamicLock(quint8 dyn_lock_byte0, quint8 dyn_lock_byte1, quint8 dyn_lock_byte2) {
    QByteArray dlb;
    dlb.append(dyn_lock_byte0);
    dlb.append(dyn_lock_byte1);
    dlb.append(dyn_lock_byte2);
    dlb.append('\x00');
    return WritePage(DYN_LOCK, dlb);
}

int Ntag213::SetMirroring(quint8 page, quint8 byte, bool uid, bool cnt, bool tt) {
    Q_UNUSED(tt);

    QByteArray cfg0; int ret;
    if((ret=Read1Page(CFG_0, cfg0)) < 0) return ret;

    quint8 m = cfg0[0];
    m |= (
        uid  << M_UID_OFFSET |
        cnt  << M_CNT_OFFSET |
        byte << M_BYTE_OFFSET);

    cfg0[0] = m;
    cfg0[2] = page;

    return WritePage(CFG_0, cfg0);
}

int Ntag213::SetAuth0(quint8 from) {
    QByteArray cfg0; int ret;
    if((ret=Read1Page(CFG_0, cfg0)) < 0) return ret;

    cfg0[3] = from;

    return WritePage(CFG_0, cfg0);
}

int Ntag213::EnableCounter(bool enable) {
    QByteArray cfg1; int ret;
    if((ret=Read1Page(CFG_1, cfg1)) < 0) return ret;

    quint8 a = cfg1[0];
    if(enable) a |= (1 << A_NFC_CNT_EN);
    else a &= ~(1 << A_NFC_CNT_EN);

    cfg1[0] = a;
    return WritePage(CFG_1, cfg1);
}

int Ntag213::SetAccess(bool prot, bool cfglck, bool nfc_cnt_prot) {
    quint8 a = 0;
    a |= (
        prot << A_PROT |
        cfglck << A_CFGLCK |
        nfc_cnt_prot << A_NFC_CNT_PWD_PROT);

    QByteArray cfg1;
    cfg1.append(a);
    cfg1.append(3, (char)0);

    return WritePage(CFG_1, cfg1);
}

int Ntag213::WritePWD(QByteArray pwd) {
    return WritePage(PWD, pwd);
}

int Ntag213::WritePACK(QByteArray pack) {
    return WritePage(PACK, pack);
}

int Ntag213::WriteCFG0Page(QByteArray data){
    return WritePage(CFG_0, data);
}
int Ntag213::WriteCFG1Page(QByteArray data){
    return WritePage(CFG_1, data);
}
